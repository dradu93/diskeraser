# simple PoC makefile for the DiskEraser application
CC=gcc
FLAGS=-Wall

SRC=DiskEraser.c
APP=diskeraser

all: build
run: distcheck

build: compile

compile: diskeraser

diskeraser: DiskEraser.c
	${CC} $^ -o ${APP} ${FLAGS}
	
distcheck: ${APP}
	./${APP} -h

.PHONY: clean
clean:
	rm -rf ${APP} ~*
