#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>

#define PAGE_SIZE 512

#define VERSION "1.0.0"

const char* program_name;

void print_usage(FILE *file, int exit_code)
{
	fprintf(file, "Usage: %s options [ ... ]\n", program_name);
	fprintf(file, "Options:\n"
			"-h --help	Display this help information\n"
			"-d --device	The target device under /dev\n"
			"-v --version	Dsiplay the application version\n");
	exit(exit_code);
}

void print_version(FILE *file, int exit_code)
{
	fprintf(file, "DiskEraser version %s\n", VERSION);
	exit(exit_code);
}

int main(int argc, char **argv)
{
	program_name = argv[0];
	if(argc < 2)
	{
		print_usage(stdout, EXIT_FAILURE);
	}

	int next_option;
	const char* const short_options = "hd:v";


	const struct option long_options[] = {
		{ "help",	0, NULL, 'h' },
		{ "device",	1, NULL, 'd' },
		{ "version",	0, NULL, 'v' },
		{ NULL,		0, NULL,  0  }
	};

	const char* device_name = NULL;
	do
	{
		next_option = getopt_long(argc, argv, short_options, long_options, NULL);

		switch(next_option)
		{
			case 'h':
				print_usage(stdout, EXIT_SUCCESS);
				break;
			case 'd':
				device_name = optarg;
				break;
			case 'v':
				print_version(stdout, EXIT_SUCCESS);
				break;
			default:
				fprintf(stdout, "Wrong usage!\n");
				print_usage(stdout, EXIT_FAILURE);
		}
	} while(next_option != -1);

	FILE *f = fopen(device_name, "r");
	if(f == NULL)
	{
		fprintf(stdout, "Error opening disk!\n");
		return 2;
	}
	fseek(f, 0, SEEK_END);
	unsigned long len = (unsigned long)ftell(f);
	fclose(f);

	printf("\e[?25l");

	int fd = open(argv[1], O_WRONLY);

	unsigned char *zero_array = (unsigned char*)calloc(1, PAGE_SIZE);

	ssize_t written, total = 0;
	long double percentage_done;
	int i = 0;
	const int cycle = (1 << 8);

	do
	{
		written = write(fd, zero_array, PAGE_SIZE);
		total += written;
		++i;
		if(cycle & i)
		{
			i ^= i;			
			percentage_done = ((double)total) / len;
			printf("\r%.4Lf%%", percentage_done);
		}
	} while(written == PAGE_SIZE);
	percentage_done = ((double)total) / len;
	printf("\r%.4Lf%%\n", percentage_done);
	close(fd);
	free(zero_array);

	fprintf(stdout, "Bytes written: %ld\n", total);

	return 0;
}
